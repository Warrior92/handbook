

# Overview

Architecture is a group of System Architects that are in charge to define projects and products architecture following the CTO guidelines.

# Process

1. At the beginning of each project, an architect is assigned to it.
2. The architect after learning the pre-existing (pre-sales ) and the functional, technical, strategic and political context defines the HLD and system sizing if not already done in the pre-sales
3. The architect defines, based on team experience and project complexity, whether an LLD is needed or not.
4. The project team following the HLD will generate the LLD if necessary
5. The LLD will be validated by the architect
6. The architect remains supportive for the duration of the project and if difficulties or unforeseen events arise, it provides help on architectural themes.
7. In some cases, the architect will need to guide system performance analyses and track them


# Accountabilities

1. Technology choice, among those defined at the business strategy level. Technology means infrastructure and application frameworks, languages, libraries and external services
2. HLD production and documentation storage
3. Choice LLD yes or no
4. LLD Validation and documentation storage


# HLD

The HLD will be represented by a power point presentation with editable diagrams. The purpose of this document is to give a logical representation to the architecture (so without expressing technologies, but only concepts ) and then to decline this in physical architecture (then choosing frameworks, components, languages and services ).
HLD also cares about breaking the system into macro-components and giving it a nomenclature.

In the document there will be a first part of context, in which will be described the functional, technical, strategic and political context.

HLD diagrams should be commented extensively in slide notes, motivating choices (because one technology instead of another, doubts and reasoning).

The document must be versioned on each iteration inside the "Architecture" sharepoint and then published also into the project specific Sharepoint.


# Conventions

All the documentation will be stored into "Architecture" sharepoint.

Folder location for HLD:    /Customer/Project 

Documents:
Naming convention:  Customer_Project_TypeOfDoc_Version_Status
Version: incremental and start from 1
Status: Draft | Final 


# Architectural Guidelines

In the "Architecture" sharepoint is easy to find a document "Guidelines.pptx" with all the reccomended frameworks, tools and languages. Also some architectural BluePrints of most frequest use cases is present.

