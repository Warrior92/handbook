
The onboarding process for the Engineering Department is the starting point for all the new people joining Agile Lab.

# Checklist for Manager

- [ ] Create an account on Office365 "name.surname@agilelab.it" and send credentials to the private email
- [ ] Include it on group "dev"
- [ ] Include it on "BigData" sharepoint
- [ ] Create an account on "elapseit" and send credentials to "agilelab.it" email
- [ ] Create an account on "gitlab" with the agile lab email and let the new team member confirm it
- [ ] Add the new member to the "developers" gitlab group
- [ ] Assign an onboarding buddy on the new employee's team.
- [ ] Send an email to the new employee and the buddy linking this page to let them be aware about next steps
- [ ] Organize smooth onboarding with clear tasks. Define which technologies should be studied in the first two weeks.
- [ ] Include it on first project Sharepoint
- [ ] Include it on recurring activities and meetings for the first project

# Checklist for Buddy

- [ ] Introduce yourself to the new team member with a video call
- [ ] Remind about the handbook, suggest most useful pages, clarify it in case of need
- [ ] Introduce it to Slack, adding it on major channels
- [ ] Check in regularly about its first tasks status and clarify doubts
- [ ] Support it on training tasks for the first two weeks.

# Checklist for new team-member

- [ ] Login into Office365 and check provided credentials are working
- [ ] Login on Elapseit and check proided credentials are working
- [ ] Login on Slack with agilelab.it account
- [ ] Check you can access to "BigData" sharepoint
- [ ] Create an account on GitLab a provide it to your Manager


# First two weeks

In Agile Lab the first two weeks are dedicated to train on different technologies based on Manager indications. Buddy will be supportive along this period.

In "BigData" Sharepoint there are the official Agile Skill courses that must not be shared with external world. This material can be then integrated with other resources in case of need.



