
# Handbook

This is the central repository about how we run the company and where all the processes are described. If you don't find something, open an issue on the handbook repository.


# Office365

Agile Lab has the full suite Office365 and it is leveraging the following features:
* Mail
* Calendar
* SharePoint
* OneDrive
* OneNote
* Office Suite: Excel - Power Point - Word


# SharePoint

Sharepoint is the **official** repository for documentation. Each team has one Sharepoint site where we store all kind of informations ( requirements, notes, documentation ) related to the customer/project. Everything is immediately shared with the team and available online, so it can be linked to gitlab or in other media.
Customer and external people will not access it, if you want to share something with the external world you have to share single files through OneDrive and protecting them with one to one sharing mode.
It is an individual responsibility to feed Sharepoint with as much information as possible, organizing documents in a searchable and understandable way.
Sharepoint sites can be automatically synchronized with your local PC, so you can work on your local file system and having background synchronization.

Also, we have the "BigData" Sharepoint that is available to all the people in AgileLab where you can find training material and interesting technical documents about a lot of technologies.


# Slack

Slack is the preferred way to communicate within the team, but if you want to be sure someone will take an action you have to write an email or open an issue on GitLab.
[Agile Lab Team](https://agilelab-team.slack.com/)


### Do Not Disturb Hours
Slack now supports "Do Not Disturb Hours" so you won't be pinged in the middle of the night or while you are dealing with family matters. You can set your "Do Not Disturb Hours" by clicking on the bell at the top of the left pane in the Slack app. You also have the option of snoozing for 20 minutes or up to 24 hours. Note: Do Not Disturb can be overridden in the event of an emergency. See Slack documentation for more information.

### Quick Switcher
Quick Switcher is a great feature to know about if you want to get productive with Slack. As the name suggests, it allows you to switch between channels and direct messages quickly. Invoke it with Cmd + k on Mac or Ctrl + k on Windows or Linux and start typing the name of the person to chat with or the channel you are interested in. You can then navigate the suggestions with ↑ and ↓ keys and hit enter to select.

### Hide conversations with no unread activity
With lots of channels and direct messages, Slack can become overwhelming. To help keep track of activity on Slack, and to simplify the interface, consider hiding conversations with no unread activity.

### Minimize Visual Distractions
Animated images and emoji can add meaning to conversation, but they can also be distracting. If you would prefer to have static images and emoji, disable the animation. For details, see Manage animated images and emoji.

### Slack Apps
[Office365 for Slack](https://agilelab-team.slack.com/apps/collection/office365-for-slack)

### Browse Channels

* `#general`: greetings, general conversation, no tech stuff
* `#random`: same as general but more oriented to off topic discussions
* `#tech`: link posting and relative discussions about technical topics
* `#thanks`: if someone or a team helped you, say thanks in public
* `#handbook`: questions and clarifications about handbook. New merge requests to the handbook will be posted always on this channel.
* `#developers`: questions and discussion about software development
* `#elapseit`: questions and discussion about timesheets.


# Discourse

This is the internal stackoverflow of Agile Lab where we store questions and answers with technical solutions ( code snippets, configurations or other ) and basically it is the technical knowledge base.
[Discourse Login](https://discourse.agilelab.it)


# Elapseit

Elapseit is the project tracking software that we use for:
- Resources Allocation
- Timesheets
- Budget Tracking
- Holidays request/approval

Timesheets must be compiled once a week ( possibly on friday ), in case you are in trouble with that, just drop a question to the dedicated slack channel #elapseit

[Elapseit](https://app.elapseit.com  )


# Other

### Krisp
Krisp will mute background noise when you're in a noisy environment so you can hear and be heard more easily on calls.

### Grammarly
Grammarly is a good tool for those who want to feel more comfortable drafting written communication in English (American or British). There is a free and premium version.
Warning: Grammarly browser extensions are discouraged, Grammarly will have access to everything you type in your browser, and they have had a security problem. If you want to use it to check non-confidential text manually, you should download the desktop version instead.

### Brain.fm
Brain.fm (free trial) provides music specially designed to help you focus, relax, meditate, recharge, sleep (great for plane rides). It's not just music though. They use scientifically validated brainwave manipulations to get results. It is AMAZING and really does work. Make sure to use with headphones, and give it 10-15 minutes for your brain to get used to it. ($6.95/$15.99/$47.40 per month/quarter/year)

### Do NOT Use
Flash: Due to security flaws, we strongly recommend not using Adobe Flash. Certainly do not install it on your local machine. But even the Google Chrome plugin that let's you see embedded Flash content in websites can pose a security hazard. If you have not already, go to your Chrome Flash Settings and disable Flash. For further context, note that Google Chrome is removing Flash support soon, and while the plugin is better than a local install of Flash, it still leaves vulnerabilities for zero-day attacks.



