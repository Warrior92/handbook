
In Agile Lab we search for smart and reliable people, we don't care about skills and past experiences. Our aim is to hire the best candidates around the world, leveraging our remote first attitude.
We are best in class ( or at least we would like to be ) in our domain, so people will learn everything is needed inside the company.

# Principles

Our hiring process reflects our company values.

- Collaboration
    *   Lean on our company for their global talent networks
    *   Develop a rich and robust referral program
    *   Foster cross-company understanding of roles and open positions
    *   Distribute ownership of team building and hiring across full company
    *   Encourage internal applications, support career transitioning
- Results
    *   Identify and source high potential candidates from varied industries
    *   Build long-term hiring forecasts and talent maps
    *   Make talent a long term priority with a deep true vision and strategy
    *   Hire the right person for the right role at the right time
    *   Transform talent from a cost center to an organizational value add
    *   Measure employee engagement and candidate experience
- Efficiency
    *   Respect candidate and employee time in hiring
    *   Create a repeatable, standard process that is well documented
    *   Support training and knowledge transfer
    *   Source for candidates in Low Cost areas
-   Trasparency
    *   Document the hiring process and keep it updated
    *   Make sure the candidate knows in advance the process and the timing
    *   Give every candidate an answer.
    *   Share insights and hiring achievements often and with everyone
    *   Provide honest and kind feedback when asked (try to separate the interview from the decision, and from the feedback)
    *   Share hiring plans internally and announce opening positions before publishing


# The Process

The recruitment process is structured in three steps:

*   **Screening Call**: 
    *   One of our recruiter will conduct a screening call using Skype
    *   This will be scheduled respecting candidate timing. 
    *   The purpose of this step is to present the company to the candidate and to understand if resume informations are consistent.
    *   Also must be ensured that there is still mutual interest before proceeding with next step. 
    *   Interview process is a considerable investment of time, so if you are not interested, please drop at this stage. Don't do the interview for curiosity or other purposes. 
    *   The recruiter will wait for 5 minute for the candidate to show up at the meeting. If the candidate does not show up to the interview or reach out in advance to reschedule, the candidate will be classified as a "no show" and be disqualified.
    *   The recruiter, hiring manager, or candidate can terminate the discussion early at any point during the interview if either party determines that it isn’t a fit. Be as transparent and honest as possible and provide feedback.

*   **Technical Interview**:
    * this is a 1.5 hour technical interview on skype
    * the interview has a theory part that will follow the format of Bar-Raiser and it will not require knowledge about specific technologies, but only general concepts.
    *   The recruiter will wait for 5 minute for the candidate to show up at the meeting. If the candidate does not show up to the interview or reach out in advance to reschedule, the candidate will be classified as a "no show" and be disqualified.
    *   The recruiter, hiring manager, or candidate can terminate the discussion early at any point during the interview if either party determines that it isn’t a fit. Be as transparent and honest as possible and provide feedback.
    *   After the interview an immediate feedback can be provided, otherwise it will come within 2 working days.

*   **CEO Interview**:
    *   this is a 30 minute behavioral interview
    *   The CEO should make an offer verbally during the call with the candidate, and it will be followed with an official contract the day after.
    *   The offer will be valid for 5 working days since the reception of the official contract.


# Interviewer Guideline

1. Maintain candidate confidentiality
2. Those on the interview team should prioritize the interview in their schedules
3. During the screening call, remind the candidate to look at this handbook
4. Compensation is discussed at start and end but not in between
5. The entire process should stay within 2 weeks


# Conducting a Screening Call

Calls can last anywhere between 10 and 30 minutes, depending on the conversation.

Example questions include:

1.  Why are you looking for a new position?
2.  Why did you apply with Agile Lab?
3.  What are you looking for in your next position?
4.  Why did you join and leave your last three positions?
5.  What is your experience with X? (for each of the skills listed in the position description)
6.  What did you learn from your last project ? ( verify alignment with resume )
6.  What is the notice period you would need if you were hired?
7.  What are your compensation expectations?

Then remind to the candidate about our handbook and tell about life in Agile Lab: values, results, projects ( without details about customer and other sensitive data ).

Conclude the call checking for mutual interest in proceeding with further steps. Usually we provide a final feedback about if you have been selected for the next step or not within 2 working days. In case of positive feedback we provide also a possible schedule for the next round.







