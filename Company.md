
# Goals

Our goals have always been the same: Have fun, do exceptional work, build the best solutions in the business, experiment, pay attention to the details, treat people right, tell the truth, have a positive impact on the world around us, give back, and keep learning.

We don’t spend more than we earn, we don’t waste money on things that don’t matter and we re-invest all the profits in our business and our people.